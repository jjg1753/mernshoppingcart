const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const items = require('./routes/api/items_api');
const app = express();
const config = require('config');
//Middleware for bodyParser
app.use(express.json());

// Database config concurrently
const db = config.get('mongoURI');

//Establishing Database Connenction
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true
  })
  .then(() => console.log('Established successful connection with MongoDB...'))
  .catch(err => console.log('Failure in establishing connection with MongoDB. Error-'+err));

//Use Routes to route to the route file
app.use('/api/items', require('./routes/api/items_api'));
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));

// Serve static assets if in production environment
if(process.env.NODE_ENV === 'production') {
  // Set a static folder to build the react front-end
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

//process environment variable in Heroku
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on Port: ${port}`));
