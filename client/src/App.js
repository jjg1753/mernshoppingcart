import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavbar from './components/AppNavbar';
import ShoppingList from './components/shoppingList';
import AddItemModal  from './components/addItemModal';
import { Container } from 'reactstrap';

import { Provider } from 'react-redux';
import store from './store';
import { loadUser } from './actions/authActions';

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return(
    <Provider store={store}>
      <div className="App">
        <AppNavbar />
        <Container>
          <AddItemModal />
          <ShoppingList />
        </Container>
      </div>
    </Provider>
)}
}
export default App;
