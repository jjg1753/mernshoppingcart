import React, { Component, Fragment } from 'react';
import{
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container
} from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import RegisterModal from './auth/registerModal';
import Logout from './auth/logout';
import LoginModal from './auth/loginModal';

class AppNavbar extends Component{
  state = {
    isOpen: false
  };

  static propTypes = {
    auth: PropTypes.object.isRequired
  }
  //The arrow directly binds the this object of the component to the method.
  //If we don't use that, then we need to use .bind(this) in the class to bind
  //this object of the component with this custom method.
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const{ isAuthenticated, user } = this.props.auth;

    const authLinks = (
      <Fragment>
        <NavItem>
          <span className="navbar-text mr-3">
            <strong>{ user ? `Welcome ${user.name}!` :'' }</strong>
          </span>
        </NavItem>
        <NavItem>
          <Logout />
        </NavItem>
      </Fragment>
    );

    const guestLinks = (
      <Fragment>
        <NavItem>
        <RegisterModal />
        </NavItem>
        <NavItem>
        <LoginModal />
        </NavItem>
      </Fragment>
    );
    return(

      <div>
        <Navbar color="dark" dark expand="sm" className="mb-5">
          <Container>
            <NavbarBrand href="/">Shopping Cart <i className="fas fa-cart-plus"></i>

</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            {/*navbar at the end to let collapse know that it is only working on navbar*/}
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                { isAuthenticated ? authLinks : guestLinks }
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </div>
  );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, null)(AppNavbar);
