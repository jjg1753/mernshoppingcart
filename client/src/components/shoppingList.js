import React, {
  Component
} from 'react';
import {
  Container,
  ListGroup,
  ListGroupItem,
  Button,
  Badge
} from 'reactstrap';
import {
  CSSTransition,
  TransitionGroup
} from 'react-transition-group';
import {
  connect
} from 'react-redux';
import {
  getItems,
  deleteItem
} from '../actions/itemActions';
import PropTypes from 'prop-types';

class ShoppingList extends Component {

  //Adding a function named getItems and an object state to the
  //properties of the component
  static propTypes = {
    getItems: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  }

  componentDidMount() {
    this.props.getItems();
  }

  onDeleteClick = id => {
    this.props.deleteItem(id);
  }

  render() {
    const {
      items
    } = this.props.item;
    return ( <Container >
      <ListGroup >
      <TransitionGroup className = "shopping-list" > {
        items.map(({
          _id,
          name,
          quantity
        }) => ( <CSSTransition key = { _id } timeout = { 500 }
          classNames = "fade" >
          <ListGroupItem className = "justify-content-between" > {
            this.props.isAuthenticated ? <
            Button className = "remove-btn"
            color = "danger"
            size = "sm"
            onClick = {
              this.onDeleteClick.bind(this, _id)
            } >&times; < /Button>:null} {name} < Badge color = "info" pill > x { quantity} < /Badge>
            < /ListGroupItem >
          </CSSTransition>
        ))
      }
      </TransitionGroup>
      </ListGroup >
      </Container>
    );
  }
}

//Using connect to get data into react component from redux state
//Using mapStateToProps to map the returned state from redux to the properties
//of the component

const mapStateToProps = (state) => ({
  item: state.item,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {
  getItems,
  deleteItem
})(ShoppingList);
