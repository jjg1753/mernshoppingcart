import React, { Component, Fragment } from 'react';
import { logout } from '../../actions/authActions';
import { connect } from 'react-redux';
import PropType from 'prop-types';
import { NavLink } from 'reactstrap';

export class Logout extends Component {
  static propTypes = {
    logout: PropType.func.isRequired
  };

  render() {
    return (
      <Fragment>
        <NavLink onClick={this.props.logout} href="#">
        <i className="fas fa-sign-out-alt" style={{paddingRight: '0.2rem'}}></i>
        Logout
        </NavLink>
      </Fragment>
    )
  }
}

export default connect(
  null,
  { logout }
)(Logout);
