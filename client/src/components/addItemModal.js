import React, {
  Component
} from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import {
  connect
} from 'react-redux';
import {
  addItem
} from '../actions/itemActions';
import PropTypes from 'prop-types';

class AddItemModal extends Component {
  state = {
    modal: false,
    name: '',
    quantity: 1
  };

  static propTypes = {
    isAuthenticated: PropTypes.bool
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();

    const newItem = {
      name: this.state.name,
      quantity: this.state.quantity
    }

    // Add item via addItem action method in props
    this.props.addItem(newItem);

    // Closing Modal
    this.toggle();
  }


  render() {
    return ( <
      div > {
        this.props.isAuthenticated ? <
        Button color = "success"
        style = {
          {
            marginBottom: '2rem',
            fontSize: 'large',
            backgroundColor: 'white',
            color: 'Green',
            border: '2px solid #4CAF50',
            textAlign: 'center'
          }
        }
        onClick = {
          this.toggle
        } >
        +Add New < /Button> : <h4 className="mb-3 ml-4"> Please Login or Signup to manage items in the cart</h4 >
      }

      <
      Modal isOpen = {
        this.state.modal
      }
      toggle = {
        this.toggle
      } >
      <
      ModalHeader toggle = {
        this.toggle
      } > Add to Shopping List < /ModalHeader> <
      ModalBody >
      <
      Form onSubmit = {
        this.onSubmit
      } >
      <
      FormGroup >
      <
      Label
      for = "item" > Item < /Label> <
      Input type = "text"
      name = "name"
      id = "item"
      placeholder = "Add shopping item"
      onChange = {
        this.onChange
      }
      /> < /
      FormGroup > <
      FormGroup >
      <
      Label
      for = "quantity" > Quantity < /Label> <
      Input type = "select"
      name = "quantity"
      id = "quantity"
      onChange = {
        this.onChange
      } >
      <
      option > 1 < /option> <
      option > 2 < /option> <
      option > 3 < /option> <
      option > 4 < /option> <
      option > 5 < /option> <
      option > 6 < /option> <
      option > 7 < /option> <
      option > 8 < /option> <
      option > 9 < /option> <
      option > 10 < /option> < /
      Input > <
      Button color = "success"
      style = {
        {
          marginTop: '2rem'
        }
      }
      block >
      Add to cart < /Button> < /
      FormGroup > <
      /Form> < /
      ModalBody > <
      /Modal> < /
      div >
    );
  }
}

const mapStateToProps = state => ({
  item: state.item,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {
  addItem
})(AddItemModal);