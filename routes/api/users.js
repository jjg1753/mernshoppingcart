const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');

// Importing user model
const UserSchema = require('../../models/user_model');

// @route Post api/users
// @desc Register new user
// @access Public
router.post('/', (req, res) => {
  const {name, email, password } = req.body;

  //Validation
  if (!name || !email || !password) {
    // Returning 400 bad request because user didn't send the correct
    // that was required by the api
    return res.status(400).json({msg: 'Please enter all fields'});
  }

  // If the request is right the we check for existing user in the Database
  UserSchema.findOne({ email })
    .then(user => {
      if(user) return res.status(400).json({ msg: ' This Email id is already registered in the database '});

      const newUser = new UserSchema({
        name,
        email,
        password
      });

      // Hashing and Salting of the password
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if(err) throw err;
          newUser.password = hash;
          newUser.save()
            .then(user => {
              //Generating the Json Web Token and adding it to the payload
              //expires time in seconds(1hr)
              jwt.sign(
                { id: user.id },
                config.get('jwtSecret'),
                { expiresIn: 3600 },
                (err, token) => {
                  if(err) throw err;
                  res.json({
                    token,
                    user: {
                      id: user.id,
                      name: user.name,
                      email: user.email
                    }
                  });
                }
              )
            });
        })
      })
    })
});

module.exports = router;
