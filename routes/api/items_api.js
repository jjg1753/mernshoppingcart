const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

// Importing item model
const Item_Model = require('../../models/Item_model');

// @route GET api/items
// @desc Get All items using express
// @access Public
router.get('/', (req, res) => {
  Item_Model.find()
    // Sorting in descending order of the date showing
    // the most recent addition to the cart first
    .sort({ date: -1 })
    .then(items => res.json(items));
});

// @route Post api/items
// @desc Post an item to database using Express
// @access Private
router.post('/', auth, (req, res) => {
  const newItem = new Item_Model({
    name: req.body.name,
    quantity: req.body.quantity
  });

  newItem.save()
    .then(item => res.json(item))
    .catch(err => console.log(err));
});

// @route Delete api/items/:id
// @desc Delete an item from the database with an id using Express
// @access Private
router
.delete('/:id', auth, (req, res) => {
  Item_Model.findById(req.params.id)
    .then(item => item.remove().then(() => res.json({ success: true})))
    .catch(err => console.log('Delete failed Error- '+err));
});


module.exports = router;
