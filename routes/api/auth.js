const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const auth = require('../../middleware/auth')

// Importing user model
const UserSchema = require('../../models/user_model');

// @route Post api/auth
// @desc Authenticate new user and Crete new user and token
// @access Public
router.post('/', (req, res) => {
  const { email, password } = req.body;

  //Validation
  if (!email || !password) {
    // Returning 400 bad request because user didn't send the correct
    // that was required by the api
    return res.status(400).json({msg: 'Please fill up all the fields'});
  }

  // If the request is right the we check for existing user in the Database
  UserSchema.findOne({ email })
    .then(user => {
      if(!user) return res.status(400).json({ msg: ' User does not exist '});

      // Validating the password first
      bcrypt.compare(password, user.password)
        .then(isMatch => {
          if(!isMatch) return res.status(400).json({ msg: 'EmailID and/or Password don\'t match. Invalid Credentials!'});

          jwt.sign(
            { id: user.id },
            config.get('jwtSecret'),
            { expiresIn: 3600 },
            (err, token) => {
              if(err) throw err;
              res.json({
                token,
                user: {
                  id: user.id,
                  name: user.name,
                  email: user.email
                }
              });
            }
          )
        })
    })
});

// @route Get api/auth/user
// @desc Get user data from the token to authenticate
// If the user has the token then the user data is sent back
// @access Private
router.get('/user', auth, (req, res) => {
  UserSchema.findById(req.user.id)
   .select('-password')
   .then(user => res.json(user));
});

module.exports = router;
