const config = require('config');
const jwt = require('jsonwebtoken');

// Now, whenever, we want a private route, we can add this
// piece of middleware to the endpoint as a second parameter for
//token verification.
// This middleware is used to give access to user to private Routes
// by authenticating their tokens
function auth(req, res, next) {
  const token = req.header('x-auth-token');

  if(!token)
    return res.status(401).json({ msg: 'Unauthorized access. No token present' });

    try {
      // Verifying the token
      const decoded = jwt.verify(token, config.get('jwtSecret'));
      // Add user from payload to the request

      req.user = decoded;
      next();
    } catch(e) {
      res.status(400).json({ msg: 'Token invalid!'});
    }
}

module.exports = auth;
